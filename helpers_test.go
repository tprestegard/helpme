package main

import (
	"os"
	"path"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetCommandPathEnvVar(t *testing.T) {
	testPath := "/fake/path"
	os.Setenv("HELPME_PATH", testPath)
	result, _ := getCommandPath()
	os.Unsetenv("HELPME_PATH")
	assert.Equal(t, testPath, result)
}

func TestGetCommandPathDefault(t *testing.T) {
	result, _ := getCommandPath()
	assert.Equal(t, path.Join(os.Getenv("HOME"), ".config", "helpme"), result)
}

func TestGetCommandPathNoHome(t *testing.T) {
	// Get current $HOME
	home := os.Getenv("HOME")

	// Temporarily unset $HOME
	err := os.Unsetenv("HOME")
	assert.NoError(t, err)
	defer os.Setenv("HOME", home)

	// Get command path and check result
	_, err2 := getCommandPath()
	assert.Error(t, err2)
	assert.Equal(t, err2.Error(), "$HOME environment variable not set")
}
