package commands

import (
	"strings"
)

// CommandArray is an array of Command objects
type CommandArray []*Command

// UnmarshalYAML is an unmarshaller for a YAML structure which may comprise a list of commands
// or a single command. The result will be an array of *Command objects.
func (ca *CommandArray) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var multi []*Command
	err := unmarshal(&multi)
	if err != nil {
		// Check error contents - "cannot unmarshal !!map" indicates it's
		// probably a single command, so if we don't see that, we can just
		// return here
		if !strings.Contains(err.Error(), "cannot unmarshal !!map") {
			return err
		}

		// Process as a single command
		var single *Command
		err2 := unmarshal(&single)
		if err2 != nil {
			return err2
		}
		*ca = []*Command{single}
	} else {
		*ca = multi
	}
	return nil
}
