package commands

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

var codeCommand *Command = &Command{
	Name:        "code-command",
	Description: "code command description",
	Code:        "some code",
}

var infoCommand *Command = &Command{
	Name:        "info-command",
	Description: "info command description",
	Info:        "some info",
}

var subcommandsCommand *Command = &Command{
	Name:        "subcommands-command",
	Description: "subcommands command description",
	Subcommands: []*Command{
		{
			Name:        "subcommand1",
			Description: "sc1 description",
			Code:        "sc1 code",
		},
		{
			Name:        "subcommand2",
			Description: "sc2 description",
			Code:        "sc2 code",
		},
	},
}

var subcommandPathsCommand *Command = &Command{
	Name:            "subcommand-paths-command",
	Description:     "subcommand paths command description",
	SubcommandPaths: []string{"path1", "path2", "path3"},
}

var invalidCommand *Command = &Command{
	Name:        "invalid-command",
	Description: "invalid command description",
	Code:        "some code",
	Info:        "some info",
	Subcommands: []*Command{
		{
			Name:        "subcommand1",
			Description: "sc1 description",
			Code:        "sc1 code",
		},
		{
			Name:        "subcommand2",
			Description: "sc2 description",
			Code:        "sc2 code",
		},
	},
	SubcommandPaths: []string{"path1", "path2", "path3"},
}

func TestHasSubcommands(t *testing.T) {
	cases := []struct {
		cmd            *Command
		expectedResult bool
	}{
		{cmd: codeCommand, expectedResult: false},
		{cmd: infoCommand, expectedResult: false},
		{cmd: subcommandsCommand, expectedResult: true},
		{cmd: subcommandPathsCommand, expectedResult: true},
		{cmd: invalidCommand, expectedResult: true},
	}
	for _, c := range cases {
		result := c.cmd.HasSubcommands()
		assert.Equal(t, c.expectedResult, result)
	}
}

func TestIsCommandGroup(t *testing.T) {
	cases := []struct {
		cmd            *Command
		expectedResult bool
	}{
		{cmd: codeCommand, expectedResult: false},
		{cmd: infoCommand, expectedResult: false},
		{cmd: subcommandsCommand, expectedResult: true},
		{cmd: subcommandPathsCommand, expectedResult: true},
		{cmd: invalidCommand, expectedResult: false},
	}
	for _, c := range cases {
		result := c.cmd.IsCommandGroup()
		assert.Equal(t, c.expectedResult, result)
	}
}

func TestIsBasicCommand(t *testing.T) {
	cases := []struct {
		cmd            *Command
		expectedResult bool
	}{
		{cmd: codeCommand, expectedResult: true},
		{cmd: infoCommand, expectedResult: true},
		{cmd: subcommandsCommand, expectedResult: false},
		{cmd: subcommandPathsCommand, expectedResult: false},
		{cmd: invalidCommand, expectedResult: false},
	}
	for _, c := range cases {
		result := c.cmd.IsBasicCommand()
		assert.Equal(t, c.expectedResult, result)
	}
}

func TestValidate(t *testing.T) {
	cases := []struct {
		cmd           *Command
		errorExpected bool
	}{
		{cmd: codeCommand, errorExpected: false},
		{cmd: infoCommand, errorExpected: false},
		{cmd: subcommandsCommand, errorExpected: false},
		{cmd: subcommandPathsCommand, errorExpected: false},
		{cmd: invalidCommand, errorExpected: true},
	}
	for _, c := range cases {
		err := c.cmd.Validate()
		if c.errorExpected {
			assert.Errorf(t, err, "Invalid command %s", c.cmd.Name)
		} else {
			assert.NoError(t, err)
		}
	}
}
