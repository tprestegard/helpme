package commands

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/urfave/cli/v2"
)

func TestCommandToCliCommandCode(t *testing.T) {
	command := &Command{
		Name:        "test-name",
		Description: "desc",
		Code:        "ls -lh",
	}
	cliCommand, err := commandToCliCommand(command, "")
	assert.NoError(t, err)
	assert.Equal(t, command.Name, cliCommand.Name)
	assert.Equal(t, command.Description, cliCommand.Usage)
	assert.NotNil(t, cliCommand.Action)
	assert.Equal(t, 1, len(cliCommand.Flags))
	assert.IsType(t, &cli.BoolFlag{}, cliCommand.Flags[0])

	names := cliCommand.Flags[0].Names()
	assert.Equal(t, len(names), 2)
	assert.Equal(t, names[0], "execute")
	assert.Equal(t, names[1], "e")
	assert.Equal(t, 0, len(cliCommand.Subcommands))
}

func TestCommandToCliCommandInfo(t *testing.T) {
	command := &Command{
		Name:        "test-name",
		Description: "desc",
		Info:        "Run Ctrl+x",
	}
	cliCommand, err := commandToCliCommand(command, "")
	assert.NoError(t, err)
	assert.Equal(t, command.Name, cliCommand.Name)
	assert.Equal(t, command.Description, cliCommand.Usage)
	assert.NotNil(t, cliCommand.Action)
	assert.Equal(t, 0, len(cliCommand.Flags))
	assert.Equal(t, 0, len(cliCommand.Subcommands))
}

func TestCommandToCliCommandGroupWithSubcommands(t *testing.T) {
	command := &Command{
		Name:        "test-group",
		Description: "group desc",
		Subcommands: []*Command{
			&Command{
				Name:        "sc1",
				Description: "desc1",
				Code:        "mkdir -p",
			},
			&Command{
				Name:        "sc2",
				Description: "desc2",
				Info:        "abcd",
			},
		},
	}
	cliCommand, err := commandToCliCommand(command, "")
	assert.NoError(t, err)
	assert.Equal(t, command.Name, cliCommand.Name)
	assert.Equal(t, command.Description, cliCommand.Usage)
	assert.Nil(t, cliCommand.Action)
	assert.Equal(t, 0, len(cliCommand.Flags))

	// Check subcommands
	assert.Equal(t, 2, len(cliCommand.Subcommands))
	for i, sc := range cliCommand.Subcommands {
		assert.Equal(t, command.Subcommands[i].Name, sc.Name)
		assert.Equal(t, command.Subcommands[i].Description, sc.Usage)
		assert.Equal(t, 0, len(sc.Subcommands))
		assert.NotNil(t, sc.Action)
	}
	assert.Equal(t, 1, len(cliCommand.Subcommands[0].Flags))
	assert.Equal(t, 0, len(cliCommand.Subcommands[1].Flags))
}

// TODO: commandToCliCommand with subcommand paths

func TestCommandToCliCommandInvalid(t *testing.T) {
	command := &Command{
		Name:        "test-name",
		Description: "desc",
		Code:        "ls -lh",
		Subcommands: []*Command{
			&Command{
				Name:        "sc1",
				Description: "desc1",
				Code:        "mkdir -p",
			},
			&Command{
				Name:        "sc2",
				Description: "desc2",
				Info:        "abcd",
			},
		},
	}
	_, err := commandToCliCommand(command, "")
	assert.Error(t, err)
	assert.EqualError(t, err, fmt.Sprintf("Command %s has invalid structure", command.Name))
}

func TestCommandToCliCommandInvalidSubcommand(t *testing.T) {
	command := &Command{
		Name:        "test-name",
		Description: "desc",
		Subcommands: []*Command{
			&Command{
				Name:        "sc1",
				Description: "desc1",
				Code:        "mkdir -p",
				Subcommands: []*Command{
					&Command{
						Name:        "sc2",
						Description: "desc2",
						Info:        "abcd",
					},
				},
			},
		},
	}
	_, err := commandToCliCommand(command, "")
	assert.Error(t, err)
	assert.EqualError(t, err, fmt.Sprintf("Command %s has invalid structure", command.Subcommands[0].Name))
}

func TestCliCommandsFromFileSubcommands(t *testing.T) {
	// Create a tempfile
	tempfile, err := ioutil.TempFile("/tmp", "helpme-testclicommandsfromfilesubcommands")
	assert.NoError(t, err)
	defer os.Remove(tempfile.Name())

	// Write data
	fileContents := `
---
- name: cmd1
  description: my first command
  code: ps ax
- name: command-group-1
  description: help with commands
  subcommands:
    - name: subcommand1
      description: sc1 description
      info: this is how
`
	_, err = tempfile.Write([]byte(fileContents))
	assert.NoError(t, err)
	tempfile.Close()

	// Get commands from file
	cliCmdArray, err2 := cliCommandsFromFile(tempfile.Name())

	// Check results
	assert.NoError(t, err2)
	assert.Equal(t, 2, len(cliCmdArray))
	assert.Equal(t, "cmd1", cliCmdArray[0].Name)
	assert.Equal(t, "my first command", cliCmdArray[0].Usage)
	assert.Equal(t, 0, len(cliCmdArray[0].Subcommands))
	assert.NotNil(t, cliCmdArray[0].Action)
	assert.Equal(t, "command-group-1", cliCmdArray[1].Name)
	assert.Equal(t, "help with commands", cliCmdArray[1].Usage)
	assert.Equal(t, 1, len(cliCmdArray[1].Subcommands))
	assert.Equal(t, "subcommand1", cliCmdArray[1].Subcommands[0].Name)
	assert.Equal(t, "sc1 description", cliCmdArray[1].Subcommands[0].Usage)
	assert.Nil(t, cliCmdArray[1].Action)
}

func TestCliCommandsFromFileSubcommandPaths(t *testing.T) {
	/*
		Command structure here is:
			- command1
			- command-group-1
				- subcommand1-1
				- subgroup1
				  - subsubcommand1
				  - subsubgroup1
				    - subcommand1
				- subcommand2-1
	*/
	// Create tempdir
	dir, err := ioutil.TempDir("/tmp", "helpme-subcommands-tempdir")
	assert.NoError(t, err)
	defer os.RemoveAll(dir)

	// Create first tempfile for subsubcommands
	tempfile1, err1 := ioutil.TempFile(dir, "helpme-subsubcommands")
	assert.NoError(t, err1)
	defer os.Remove(tempfile1.Name())

	// Write data
	fileContents1 := `
---
- name: subsubcommand1
  description: ssc1 description
  code: blah blah
- name: subsubgroup1
  description: a subcommand group
  subcommands:
    - name: subcommand1
      description: sc1 description
      info: this is how
`
	_, err2 := tempfile1.Write([]byte(fileContents1))
	assert.NoError(t, err2)
	tempfile1.Close()

	// Create second tempfile for subcommands
	tempfile2, err3 := ioutil.TempFile(dir, "helpme-subcommands1")
	assert.NoError(t, err3)
	defer os.Remove(tempfile2.Name())

	// Write data
	fileContents2 := `
---
- name: subcommand1-1
  description: sc1-1 description
  code: blah blah
- name: subgroup1
  description: a subcommand group
  subcommand_paths:
    - ` + tempfile1.Name()
	_, err4 := tempfile2.Write([]byte(fileContents2))
	assert.NoError(t, err4)
	tempfile2.Close()

	// Create third tempfile for subcommands
	tempfile3, err5 := ioutil.TempFile(dir, "helpme-subcommands2")
	assert.NoError(t, err5)
	defer os.Remove(tempfile3.Name())

	// Write data
	fileContents3 := `
---
name: subcommand2-1
description: sc2-1 description
info: here is the info
`
	_, err6 := tempfile3.Write([]byte(fileContents3))
	assert.NoError(t, err6)
	tempfile3.Close()

	// Create a tempfile for base commands
	tempfile, err7 := ioutil.TempFile("/tmp", "helpme-base-commands")
	assert.NoError(t, err7)
	defer os.Remove(tempfile.Name())

	// Get path for tempfile3 relative to base dir (/tmp)
	tempfile3RelativePath := strings.SplitAfterN(tempfile3.Name(), "/", 3)[2]

	// Write data
	fileContents := `
---
- name: command1
  description: my first command
  code: command1 code
- name: command-group-1
  description: help with commands
  subcommand_paths:
    - ` + tempfile2.Name() + "\n    - " + tempfile3RelativePath

	_, err8 := tempfile.Write([]byte(fileContents))
	assert.NoError(t, err8)
	tempfile.Close()

	// Get commands from file
	cliCmdArray, err9 := cliCommandsFromFile(tempfile.Name())

	// Check results
	assert.NoError(t, err9)
	assert.Equal(t, 2, len(cliCmdArray))
}

func TestCliCommandsFromFileMissingFile(t *testing.T) {
	// Get commands from file
	filename := "/fake/file.yaml"
	_, err := cliCommandsFromFile(filename)

	// Check result
	assert.Error(t, err)
	assert.Contains(t, err.Error(), fmt.Sprintf("Error reading file %s", filename))
}

func TestCliCommandsFromFileBadArrayCommand(t *testing.T) {
	// Create a tempfile
	tempfile, err := ioutil.TempFile("/tmp", "helpme-testclicommandsfromfilesubcommands")
	assert.NoError(t, err)
	defer os.Remove(tempfile.Name())

	// Write data
	fileContents := `---
- name: new-command1
  description: cmd1 description
  code: blah blah
- name: group1
  description: a command group
  data: 1 + 2
`
	_, err = tempfile.Write([]byte(fileContents))
	assert.NoError(t, err)
	tempfile.Close()

	// Get commands from file
	_, err = cliCommandsFromFile(tempfile.Name())

	// Check result
	assert.Error(t, err)
	assert.Contains(t, err.Error(), fmt.Sprintf("Error unmarshaling file %s", tempfile.Name()))
	assert.Contains(t, err.Error(), "line 7: field data not found")
}

func TestCliCommandsFromFileBadSingleCommand(t *testing.T) {
	// Create a tempfile
	tempfile, err := ioutil.TempFile("/tmp", "helpme-testclicommandsfromfilesubcommands")
	assert.NoError(t, err)
	defer os.Remove(tempfile.Name())

	// Write data
	fileContents := `---
name: new-command1
description: cmd1 description
code: blah blah
code: abcde
`
	_, err = tempfile.Write([]byte(fileContents))
	assert.NoError(t, err)
	tempfile.Close()

	// Get commands from file
	_, err = cliCommandsFromFile(tempfile.Name())

	// Check result
	assert.Error(t, err)
	assert.Contains(t, err.Error(), fmt.Sprintf("Error unmarshaling file %s", tempfile.Name()))
	assert.Contains(t, err.Error(), "line 5: field code already set")
}

func TestGetSubcommandsSingleFile(t *testing.T) {
	// Create tempfile
	tempfile, err := ioutil.TempFile("/tmp", "helpme-getsubcommands")
	assert.NoError(t, err)
	defer os.Remove(tempfile.Name())

	// Write data
	fileContents := `
---
- name: new-command1
  description: cmd1 description
  code: blah blah
- name: group1
  description: a command group
  subcommands:
    - name: subcommand1
      description: sc1 description
      info: this is how
`
	_, err = tempfile.Write([]byte(fileContents))
	assert.NoError(t, err)
	tempfile.Close()

	// Get commands
	cmds, err2 := GetSubcommands(tempfile.Name())

	// Check results
	assert.NoError(t, err2)
	assert.Equal(t, 2, len(cmds))
	assert.Equal(t, "group1", cmds[0].Name)
	assert.Equal(t, "new-command1", cmds[1].Name)
}

func TestGetSubcommandsDir(t *testing.T) {
	// Create tempdir
	dir, err := ioutil.TempDir("/tmp", "helpme-tempdir")
	assert.NoError(t, err)
	defer os.RemoveAll(dir)

	// Create tempfile
	tempfile1, err := ioutil.TempFile(dir, "helpme-getsubcommands1-*.yml")
	assert.NoError(t, err)
	defer os.Remove(tempfile1.Name())

	// Write data
	fileContents1 := `
---
- name: new-command1
  description: cmd1 description
  code: blah blah
- name: group1
  description: a command group
  subcommands:
    - name: subcommand1
      description: sc1 description
      info: this is how
`
	_, err = tempfile1.Write([]byte(fileContents1))
	assert.NoError(t, err)
	tempfile1.Close()

	// Create tempfile2
	tempfile2, err2 := ioutil.TempFile(dir, "helpme-getsubcommands2-*.yaml")
	assert.NoError(t, err2)
	defer os.Remove(tempfile2.Name())

	// Write data
	fileContents2 := `
---
name: a-great-command
description: great description
info: information
`
	_, err = tempfile2.Write([]byte(fileContents2))
	assert.NoError(t, err)
	tempfile2.Close()

	// Create tempfile 3 (bad extension, should be skipped)
	tempfile3, err3 := ioutil.TempFile(dir, "helpme-getsubcommands2-*.badext")
	assert.NoError(t, err3)
	defer os.Remove(tempfile3.Name())

	// Write data
	fileContents3 := `
---
name: third-command
description: third description
info: more information
`
	_, err = tempfile3.Write([]byte(fileContents3))
	assert.NoError(t, err)
	tempfile3.Close()

	// Get commands
	cmds, err4 := GetSubcommands(dir)

	// Check results
	assert.NoError(t, err4)
	assert.Equal(t, 3, len(cmds))
	assert.Equal(t, "a-great-command", cmds[0].Name)
	assert.Equal(t, "group1", cmds[1].Name)
	assert.Equal(t, "new-command1", cmds[2].Name)
}

func TestGetSubcommandsFileDoesNotExist(t *testing.T) {
	// Get commands
	filename := "/fake/file.yaml"
	_, err := GetSubcommands(filename)

	// Check results
	assert.Error(t, err)
	assert.Contains(t, err.Error(), fmt.Sprintf("Unable to stat command path %s", filename))
}

func TestRunCommandCode(t *testing.T) {
	// Set up mock context
	set := flag.NewFlagSet("test", 0)
	ctx := cli.NewContext(nil, set, nil)

	// Set up command
	cmd := &Command{
		Name:        "test-name",
		Description: "desc",
		Code:        "ls -lh",
	}

	// Divert stdout
	var buf bytes.Buffer
	log.SetOutput(&buf)

	// Run
	err := RunCommand(cmd)(ctx)
	assert.NoError(t, err)

	// Get output and check
	log.SetOutput(os.Stderr)
	assert.Equal(t, cmd.Code, strings.TrimSpace(buf.String()))
}

func TestRunCommandCodeExecute(t *testing.T) {
	// Create a temporary directory
	dir, err := ioutil.TempDir("", "helpme-subcommands-tempdir")
	assert.NoError(t, err)
	defer os.RemoveAll(dir)

	// Set up mock context with execute set
	set := flag.NewFlagSet("test", 0)
	set.Bool("execute", true, "doc")
	ctx := cli.NewContext(nil, set, nil)

	// Set up command
	cmd := &Command{
		Name:        "test-name2",
		Description: "desc2",
		Code:        fmt.Sprintf("cd %s; pwd", dir),
	}

	// Divert stdout
	var buf bytes.Buffer
	log.SetOutput(&buf)

	// Run
	err = RunCommand(cmd)(ctx)
	assert.NoError(t, err)

	// Get output and check
	log.SetOutput(os.Stderr)
	assert.Equal(t, dir, strings.TrimSpace(buf.String()))
}

func TestRunCommandCodeExecuteBadCommand(t *testing.T) {
	// Set up mock context with execute set
	set := flag.NewFlagSet("test", 0)
	set.Bool("execute", true, "doc")
	ctx := cli.NewContext(nil, set, nil)

	// Set up command
	cmd := &Command{
		Name:        "test-name2",
		Description: "desc2",
		Code:        "abcd",
	}

	// Divert stdout
	var buf bytes.Buffer
	log.SetOutput(&buf)

	// Run
	err := RunCommand(cmd)(ctx)
	assert.EqualError(t, err, "exit status 127")
}

func TestRunCommandInfo(t *testing.T) {
	// Set up mock context
	set := flag.NewFlagSet("test", 0)
	ctx := cli.NewContext(nil, set, nil)

	// Set up command
	cmd := &Command{
		Name:        "test-name2",
		Description: "desc2",
		Info:        "Do Ctrl+x",
	}

	// Divert stdout
	var buf bytes.Buffer
	log.SetOutput(&buf)

	// Run
	err := RunCommand(cmd)(ctx)
	assert.NoError(t, err)

	// Get output and check
	log.SetOutput(os.Stderr)
	assert.Equal(t, cmd.Info, strings.TrimSpace(buf.String()))
}

func TestRunCommandInvalid(t *testing.T) {
	// Set up mock context
	set := flag.NewFlagSet("test", 0)
	ctx := cli.NewContext(nil, set, nil)

	// Set up command
	cmd := &Command{
		Name:        "test-name3",
		Description: "desc3",
	}

	// Run
	err := RunCommand(cmd)(ctx)
	assert.EqualError(t, err, fmt.Sprintf("Invalid command %s", cmd.Name))
}
