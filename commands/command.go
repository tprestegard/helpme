package commands

import (
	"fmt"
)

// Command struct
type Command struct {
	Name            string     `yaml:"name"`
	Description     string     `yaml:"description"`
	Code            string     `yaml:"code,omitempty"`
	Info            string     `yaml:"info,omitempty"`
	SubcommandPaths []string   `yaml:"subcommand_paths,omitempty"`
	Subcommands     []*Command `yaml:"subcommands,omitempty"`
}

// HasSubcommands returns a boolean indicating whether the Command has subcommands.
func (c *Command) HasSubcommands() bool {
	return (len(c.SubcommandPaths) > 0 || len(c.Subcommands) > 0)
}

// IsCommandGroup returns a boolean indicating whether the Command is a "group"; i.e.,
// whether it has a list of subcommands, or code/info.
func (c *Command) IsCommandGroup() bool {
	return c.Code == "" && c.Info == "" && c.HasSubcommands()
}

// IsBasicCommand returns a boolean indicating whether the Command is a simple command;
// i.e., whether it has code or info, or just a list of subcommands.
func (c *Command) IsBasicCommand() bool {
	return (c.Code != "" || c.Info != "") && !c.HasSubcommands()
}

// Validate checks that a command is either a command group or a basic command
func (c *Command) Validate() error {
	if !(c.IsCommandGroup() || c.IsBasicCommand()) {
		return fmt.Errorf("Invalid command %s", c.Name)
	}
	return nil
}
