package commands

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"sort"
	"strings"

	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v2"
)

// GetSubcommands loops over files in the command path (if a directory)
// or loads the single file to load first-level subcommands for the main app
func GetSubcommands(cmdPath string) ([]*cli.Command, error) {
	// Check if cmdPath exists; if not return with an error
	pathInfo, err := os.Stat(cmdPath)
	if err != nil {
		return nil, fmt.Errorf("Unable to stat command path %s: %s", cmdPath, err)
	}

	// If the path points to a single file, just load commands from that file and return
	if !pathInfo.IsDir() {
		cmds, err := cliCommandsFromFile(cmdPath)
		if err != nil {
			return nil, err
		}
		sort.Sort(cli.CommandsByName(cmds))
		return cmds, nil
	}

	// Otherwise, get a list of all files in the directory
	files, err2 := ioutil.ReadDir(cmdPath)
	if err2 != nil {
		return nil, err2
	}

	// Loop over files in the directory
	subcommands := []*cli.Command{}
	for _, file := range files {
		// Ignore files that don't end in .yaml or .yml
		if !(strings.HasSuffix(file.Name(), ".yaml") || strings.HasSuffix(file.Name(), ".yml")) {
			continue
		}

		// Load commands from the file
		cmds, err := cliCommandsFromFile(path.Join(cmdPath, file.Name()))
		if err != nil {
			return nil, err
		}

		// Append to the list of subcommands
		subcommands = append(subcommands, cmds...)
	}

	// Sort commands by name
	sort.Sort(cli.CommandsByName(subcommands))

	return subcommands, nil
}

// cliCommandsFromFile loads a YAML file and parses it into an array of CLI commands,
// with recursive loading of subcommands in separate files
func cliCommandsFromFile(yamlPath string) ([]*cli.Command, error) {
	// Read file
	b, err := ioutil.ReadFile(yamlPath)
	if err != nil {
		return nil, fmt.Errorf("Error reading file %s: %s", yamlPath, err)
	}

	// Parse into YAML
	cmdArray := CommandArray{}
	err = yaml.UnmarshalStrict(b, &cmdArray)
	if err != nil {
		return nil, fmt.Errorf("Error unmarshaling file %s: %s", yamlPath, err)
	}

	// Loop over commands
	cliCommandArray := []*cli.Command{}
	for _, cmd := range cmdArray {
		absPath, err := filepath.Abs(yamlPath)
		if err != nil {
			return nil, err
		}
		cliCommand, err2 := commandToCliCommand(cmd, absPath)
		if err2 != nil {
			return nil, err2
		}
		cliCommandArray = append(cliCommandArray, cliCommand)
	}

	return cliCommandArray, nil
}

// filepath should be an absolute path
func commandToCliCommand(cmd *Command, filePath string) (*cli.Command, error) {
	cliCmd := cli.Command{
		Name:  cmd.Name,
		Usage: cmd.Description,
	}
	if cmd.IsBasicCommand() {
		cliCmd.Action = RunCommand(cmd)
		if cmd.Code != "" {
			cliCmd.Flags = []cli.Flag{
				&cli.BoolFlag{
					Name:    "execute",
					Value:   false,
					Aliases: []string{"e"},
				},
			}
		}
	} else if cmd.IsCommandGroup() {
		subcommands := []*cli.Command{}

		// Loop over subcommands
		for _, sc := range cmd.Subcommands {
			cliSubcommand, err := commandToCliCommand(sc, filePath)
			if err != nil {
				return nil, err
			}
			subcommands = append(subcommands, cliSubcommand)
		}

		// Loop over subcommand paths
		for _, scp := range cmd.SubcommandPaths {
			if !filepath.IsAbs(scp) {
				scp = path.Join(path.Dir(filePath), scp)
			}
			subcommandArray, err := cliCommandsFromFile(scp)
			if err != nil {
				return nil, err
			}
			subcommands = append(subcommands, subcommandArray...)
		}
		sort.Sort(cli.CommandsByName(subcommands))
		cliCmd.Subcommands = subcommands
	} else {
		return nil, fmt.Errorf("Command %s has invalid structure", cmd.Name)
	}
	return &cliCmd, nil
}

// RunCommand executes a command
func RunCommand(cmd *Command) func(*cli.Context) error {
	return func(c *cli.Context) error {
		log.SetFlags(0)
		if cmd.Code != "" {
			if c.Bool("execute") {
				// Execute code with sh and print result
				out, err := exec.Command("/bin/sh", "-c", cmd.Code).Output()
				if err != nil {
					return err
				}
				log.Println(string(out[:]))
			} else {
				// Print code
				log.Println(cmd.Code)
			}
		} else if cmd.Info != "" {
			// Print info
			log.Println(cmd.Info)
		} else {
			return fmt.Errorf("Invalid command %s", cmd.Name)
		}
		return nil
	}
}
