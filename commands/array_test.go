package commands

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/yaml.v2"
)

func TestUnmarshalSingleCommandWithCode(t *testing.T) {
	commandYaml := `---
name: my-command
description: a description
code: ls -lh
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 1, len(cmdArray))
	assert.NoError(t, err)
	cmd := cmdArray[0]
	assert.Equal(t, cmd.Name, "my-command")
	assert.Equal(t, cmd.Description, "a description")
	assert.Equal(t, cmd.Code, "ls -lh")
	assert.Equal(t, cmd.Info, "")
	assert.Equal(t, len(cmd.Subcommands), 0)
	assert.Equal(t, len(cmd.SubcommandPaths), 0)
}

func TestUnmarshalSingleCommandWithInfo(t *testing.T) {
	commandYaml := `---
name: new-command-123
description: another description
info: Ctrl+b x
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 1, len(cmdArray))
	assert.NoError(t, err)
	cmd := cmdArray[0]
	assert.Equal(t, cmd.Name, "new-command-123")
	assert.Equal(t, cmd.Description, "another description")
	assert.Equal(t, cmd.Code, "")
	assert.Equal(t, cmd.Info, "Ctrl+b x")
	assert.Equal(t, len(cmd.Subcommands), 0)
	assert.Equal(t, len(cmd.SubcommandPaths), 0)
}

func TestUnmarshalSingleCommandWithSubcommands(t *testing.T) {
	commandYaml := `---
name: cmd3
description: desc
subcommands:
  - name: sc1
    description: scdesc1
    code: code1
  - name: sc2
    description: scdesc2
    code: code2
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 1, len(cmdArray))
	assert.NoError(t, err)
	cmd := cmdArray[0]
	assert.Equal(t, cmd.Name, "cmd3")
	assert.Equal(t, cmd.Description, "desc")
	assert.Equal(t, cmd.Code, "")
	assert.Equal(t, cmd.Info, "")
	assert.Equal(t, len(cmd.Subcommands), 2)
	assert.Equal(t, len(cmd.SubcommandPaths), 0)
	assert.Equal(t, cmd.Subcommands[0].Name, "sc1")
	assert.Equal(t, cmd.Subcommands[0].Description, "scdesc1")
	assert.Equal(t, cmd.Subcommands[0].Code, "code1")
	assert.Equal(t, cmd.Subcommands[1].Name, "sc2")
	assert.Equal(t, cmd.Subcommands[1].Description, "scdesc2")
	assert.Equal(t, cmd.Subcommands[1].Code, "code2")
}

func TestUnmarshalSingleCommandWithSubcommandPaths(t *testing.T) {
	commandYaml := `---
name: cmd4
description: desc
subcommand_paths:
  - path1
  - path2
  - path3
  - another/path
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 1, len(cmdArray))
	assert.NoError(t, err)
	cmd := cmdArray[0]
	assert.Equal(t, cmd.Name, "cmd4")
	assert.Equal(t, cmd.Description, "desc")
	assert.Equal(t, cmd.Code, "")
	assert.Equal(t, cmd.Info, "")
	assert.Equal(t, len(cmd.Subcommands), 0)
	assert.Equal(t, len(cmd.SubcommandPaths), 4)
	assert.Equal(t, cmd.SubcommandPaths[0], "path1")
	assert.Equal(t, cmd.SubcommandPaths[1], "path2")
	assert.Equal(t, cmd.SubcommandPaths[2], "path3")
	assert.Equal(t, cmd.SubcommandPaths[3], "another/path")
}

func TestUnmarshalMultipleCommands(t *testing.T) {
	commandYaml := `---
- name: cmd1
  description: desc
  code: ls -lha
- name: cmd2
  description: desc2
  info: abcd
- name: cmd3
  description: desc3
  subcommands:
    - name: cmd3-1
      description: desc3-1
      code: jklm
    - name: cmd3-2
      description: desc3-2
      info: information
- name: cmd4
  description: desc4
  subcommand_paths:
    - path001
    - path002
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 4, len(cmdArray))
	assert.NoError(t, err)
	assert.Equal(t, cmdArray[0].Name, "cmd1")
	assert.Equal(t, cmdArray[0].Description, "desc")
	assert.Equal(t, cmdArray[0].Code, "ls -lha")
	assert.Equal(t, cmdArray[0].Info, "")
	assert.Equal(t, len(cmdArray[0].Subcommands), 0)
	assert.Equal(t, len(cmdArray[0].SubcommandPaths), 0)
	assert.Equal(t, cmdArray[1].Name, "cmd2")
	assert.Equal(t, cmdArray[1].Description, "desc2")
	assert.Equal(t, cmdArray[1].Code, "")
	assert.Equal(t, cmdArray[1].Info, "abcd")
	assert.Equal(t, len(cmdArray[1].Subcommands), 0)
	assert.Equal(t, len(cmdArray[1].SubcommandPaths), 0)
	assert.Equal(t, cmdArray[2].Name, "cmd3")
	assert.Equal(t, cmdArray[2].Description, "desc3")
	assert.Equal(t, cmdArray[2].Code, "")
	assert.Equal(t, cmdArray[2].Info, "")
	assert.Equal(t, len(cmdArray[2].Subcommands), 2)
	assert.Equal(t, cmdArray[2].Subcommands[0].Name, "cmd3-1")
	assert.Equal(t, cmdArray[2].Subcommands[0].Description, "desc3-1")
	assert.Equal(t, cmdArray[2].Subcommands[0].Code, "jklm")
	assert.Equal(t, cmdArray[2].Subcommands[0].Info, "")
	assert.Equal(t, cmdArray[2].Subcommands[1].Name, "cmd3-2")
	assert.Equal(t, cmdArray[2].Subcommands[1].Description, "desc3-2")
	assert.Equal(t, cmdArray[2].Subcommands[1].Code, "")
	assert.Equal(t, cmdArray[2].Subcommands[1].Info, "information")
	assert.Equal(t, len(cmdArray[2].SubcommandPaths), 0)
	assert.Equal(t, cmdArray[3].Name, "cmd4")
	assert.Equal(t, cmdArray[3].Description, "desc4")
	assert.Equal(t, cmdArray[3].Code, "")
	assert.Equal(t, cmdArray[3].Info, "")
	assert.Equal(t, len(cmdArray[3].Subcommands), 0)
	assert.Equal(t, len(cmdArray[3].SubcommandPaths), 2)
	assert.Equal(t, cmdArray[3].SubcommandPaths[0], "path001")
	assert.Equal(t, cmdArray[3].SubcommandPaths[1], "path002")
}

func TestUnmarshalCommandBadField(t *testing.T) {
	commandYaml := `---
name: my-command
description: a description
otherfield: some words
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 0, len(cmdArray))
	assert.Error(t, err)
}

func TestUnmarshalCommandDuplicateField(t *testing.T) {
	commandYaml := `---
name: my-command
description: a description
code: ls -lh
code: pwd
`
	// Set up CommandArray and unmarshal file into it
	cmdArray := CommandArray{}
	err := yaml.UnmarshalStrict([]byte(commandYaml), &cmdArray)

	// Check result
	assert.Equal(t, 0, len(cmdArray))
	assert.Error(t, err)
}
