package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/tprestegard/helpme/commands"
)

func main() {
	// Set log flags
	log.SetFlags(0)

	// Get default path to directory containing commands
	cmdPath, err := getCommandPath()
	if err != nil {
		log.Fatal(err)
	}

	// Compile subcommands
	subcommands, err2 := commands.GetSubcommands(cmdPath)
	if err2 != nil {
		log.Fatal(err2)
	}

	// Set up main CLI
	app := &cli.App{
		Name:     "helpme",
		Usage:    "Help! Set the $HELPME_PATH environment variable to set the path to a file or directory to load commands from (default: $HOME/.config/helpme).",
		Commands: subcommands,
	}

	// Run
	err3 := app.Run(os.Args)
	if err3 != nil {
		log.Fatal(err)
	}
}
