package main

import (
	"errors"
	"os"
	"path"
)

// getCommandPath determines the path to look in for command files. Can be a single
// file or a directory.
func getCommandPath() (string, error) {
	rootPath := os.Getenv("HELPME_PATH")
	if rootPath != "" {
		return rootPath, nil
	}

	// Get default path - $HOME/.config/helpme.yaml
	home := os.Getenv("HOME")
	if home == "" {
		return "", errors.New("$HOME environment variable not set")
	}
	result := path.Join(home, ".config", "helpme")
	return result, nil
}
