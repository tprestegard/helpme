module gitlab.com/tprestegard/helpme

go 1.13

require (
	github.com/golang/mock v1.4.3 // indirect
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.2.0
	gopkg.in/yaml.v2 v2.2.2
)
