# helpme
A golang binary for quickly looking up recorded information.
`helpme` parses a YAML structure of user-provided data to produce a CLI with nested subcommands.
Each subcommand either contains a nested structure of additional subcommands, or a string of code or general information.
Subcommands which define code can be executed using the `--execute` flag.

## Example

```
# $HOME/.config/helpme/main.yaml
---
- name: git
  description: Help with git commands
  subcommands:
    - name: tags
      description: Help with git tags
      subcommands:
        - name: push-tags-to-remote
          description: Push local tags to remote
          code: git push --tags
- name: show-rfc-date
  description: Show current date and time in RFC 3339 format
  code: date -R
- name: tmux
  description: Help with tmux commands
  subcommands:
    - name: kill-pane
      description: Kill the current pane
      info: Ctrl+b x
```

```
$ helpme git tags push-tags-to-remote
git push --tags
```

```
$ helpme show-rfc-date --execute
Wed, 21 Oct 2020 20:34:43 -0700
```

```
$ helpme tmux kill-pane
Ctrl+b x
```

## Command structure
TBD

## TODO
* Allow multiple values in HELPME_PATH (colon-delimited?)
* Allow templating in command code
* Add command completion
